from django.urls import path
from . import views

app_name = 'koloid'

urlpatterns = [
    path('', views.index, name='index'),
    path('materi/', views.materi, name='materi'),
    path('sifat-koloid/', views.sifatkoloid, name='sifatkoloid'),
    path('jenis-koloid/', views.jeniskoloid, name='jeniskoloid'),
    path('pembuatan-koloid/', views.pembuatankoloid, name='pembuatankoloid'),
    path('soal/', views.soal, name='soal'),
    path('video/', views.video, name='video'),
    path('games/', views.games, name='games'),
    path('tentang-kami/', views.tentangkami, name= 'tentangkami'),
]