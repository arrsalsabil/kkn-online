from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')
def materi(request):
    return render(request, 'materi.html')
def sifatkoloid(request):
    return render(request, 'sifatkoloid.html')
def jeniskoloid(request):
    return render(request, 'jeniskoloid.html')
def pembuatankoloid(request):
    return render(request, 'pembuatankoloid.html')
def soal(request):
    return render(request, 'soal.html')
def video(request):
    return render(request, 'video.html')
def games(request):
    return render(request, 'games.html')
def tentangkami(request):
    return render(request, 'tentangkami.html')