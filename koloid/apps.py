from django.apps import AppConfig


class KoloidConfig(AppConfig):
    name = 'koloid'
